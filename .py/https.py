# hosts basic https python server, useful for some exploits, python -m http.server <port> for basic http server.
# keys must be generated
# thanks Michael Foukarakis - https://stackoverflow.com/users/149530/michael-foukarakis

import http.server, ssl

server_address = ('localhost', 4443)
httpd = http.server.HTTPServer(server_address, http.server.SimpleHTTPRequestHandler)
httpd.socket = ssl.wrap_socket(httpd.socket,
                               server_side=True,
                               certfile='localhost.pem',
                               ssl_version=ssl.PROTOCOL_TLS)
httpd.serve_forever()
